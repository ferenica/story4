from django.urls import path
from . import views

app_name = 'story4'

urlpatterns = [
    path('', views.home, name='home'),
    path('about/', views.about, name='about'),
    path('experience/', views.experience, name='experience'),
    path('skill&interest/', views.skill_int, name='skill_int'),
    path('gallery/', views.gallery, name='gallery'),
    path('contact/', views.contact, name='contact')
]